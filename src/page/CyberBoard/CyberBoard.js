import React from "react";
import Main from "../../component/Main";
import Modal from "../../component/Modal/Modal";

export default function CyberBoard() {
  return (
    <div>
      <Main />
      <Modal />
    </div>
  );
}
