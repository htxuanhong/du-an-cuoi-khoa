import React, { useEffect, useState } from "react";
import { useDispatch } from "react-redux";
import { useParams } from "react-router-dom/cjs/react-router-dom.min";
import Main from "../../component/Main";
import Modal from "../../component/Modal/Modal";
import { setHideSpinner, setShowSpinner } from "../../redux/action/action";
import { projectService } from "../../service/projectService";

export default function ProjectDetail(props) {
  const params = useParams();
  const dispatch = useDispatch();
  const [detail, setDetail] = useState();

  console.log("detail", detail);
  useEffect(() => {
    if (params?.projectId) {
      dispatch(setShowSpinner());
      projectService
        .getProjectDetail(params.projectId)
        .then((res) => {
          dispatch(setHideSpinner());
          console.log(res);
          setDetail(res.data.content);
        })
        .catch((err) => {
          dispatch(setHideSpinner());
          console.log(err);
        });
    }
  }, [params.projectId]);

  return (
    <div>
      <Main projectDetail={detail} />
      <Modal />
    </div>
  );
}
