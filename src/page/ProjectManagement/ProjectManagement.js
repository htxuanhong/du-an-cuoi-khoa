import { DeleteOutlined, FormOutlined } from "@ant-design/icons";
import {
  AutoComplete,
  Avatar,
  Button,
  message,
  Popover,
  Space,
  Table,
  Tag,
} from "antd";
import { debounce } from "lodash";
import React, { useEffect, useState } from "react";
import { useDispatch } from "react-redux";
import { NavLink } from "react-router-dom";
import DeleteProject from "../../component/DeleteProject/DeleteProject";
import FormEditProject from "../../component/FormEditProject/FormEditProject";
import {
  editProject,
  openFormEdit,
  setHideSpinner,
  setShowSpinner,
} from "../../redux/action/action";
import { projectService } from "../../service/projectService";
import { userService } from "../../service/userService";

const INIT = "khoi-tao-danh-sach";
const LOADING = "dang-tai-danh-sach";
const SUCCESS = "thanh-cong";
const FAILED = "that-bai";

const ProjectManagement = () => {
  const [dataRaw, setDataRaw] = useState([]);
  const [filteredInfo, setFilteredInfo] = useState([]);
  const [sortedInfo, setSortedInfo] = useState({});

  const [isListState, setListState] = useState(INIT);
  const [userOptions, setUserOptions] = useState([]);
  const [value, setValue] = useState("");

  let dispatch = useDispatch();
  useEffect(() => {
    if (isListState === INIT) {
      dispatch(setShowSpinner());
      setListState(LOADING);

      projectService
        .getListProject()
        .then((res) => {
          dispatch(setHideSpinner());
          setDataRaw(res.data.content);
          console.log("lay dung lieu thanh cong", res);
          console.log("chunk", res.data.content);

          setListState(SUCCESS);
        })
        .catch((err) => {
          dispatch(setHideSpinner());
          console.log(err);

          setListState(FAILED);
        });
    }
  }, [isListState]);

  const handleChange = (pagination, filters, sorter) => {
    console.log("Various parameters", pagination, filters, sorter);
    setFilteredInfo(filters);
    setSortedInfo(sorter);
  };

  const clearFilters = () => {
    setFilteredInfo({});
  };

  const clearAll = () => {
    setFilteredInfo({});
    setSortedInfo({});
  };

  const setAgeSort = () => {
    setSortedInfo({
      order: "descend",
      columnKey: "age",
    });
  };

  const searchDelayAction = debounce((keyword) => {
    userService
      .getUser(keyword)
      .then((res) => {
        console.log(res.data.content);
        if (Array.isArray(res.data.content)) {
          setUserOptions(res.data.content);
        }
      })
      .catch((err) => {
        console.log(err);
      });
  }, 500);

  const columns = [
    {
      title: "id",
      dataIndex: "id",
      key: "id",
      sorter: (item2, item1) => item2.id - item1.id, // a là phần tử thứ 2 , b là phần tử thứ nhất
      sortDirections: ["descend"],
    },

    {
      title: "projectName",
      dataIndex: "projectName",
      key: "projectName",
      render: (text, record, index) => {
        return <NavLink to={`/projectdetail/${record.id}`}>{text}</NavLink>;
      },
    },
    {
      title: "categoryName",
      dataIndex: "categoryName",
      key: "categoryName",
    },
    {
      title: "creator",
      key: "creator",
      render: (text, record, index) => {
        return <Tag color="green">{record.creator?.name}</Tag>;
      },
    },
    {
      title: "members",
      key: "members",
      render: (text, record, index) => {
        return (
          <div>
            {record.members?.slice(0, 2).map((member, index) => {
              return (
                <Popover
                  key={index}
                  placement="top"
                  title="members"
                  content={() => {
                    return (
                      <table className="table">
                        <thead>
                          <tr>
                            <th>Id</th>
                            <th>Avatar</th>
                            <th>Name</th>
                            <th></th>
                          </tr>
                        </thead>
                        <tbody>
                          {record.members?.map((item, index) => {
                            return (
                              <tr key={index}>
                                <td>{item.userId}</td>
                                <td>
                                  <img
                                    src={item.avatar}
                                    alt=""
                                    className="w-8 h-8 rounded-full"
                                  />
                                </td>
                                <td>{item.name}</td>
                                <td>
                                  <button
                                    onClick={() => {
                                      const projectId = record.id;
                                      const userId = item.userId;
                                      dispatch(setShowSpinner());
                                      userService
                                        .removeUserFromProject({
                                          projectId,
                                          userId,
                                        })
                                        .then((res) => {
                                          dispatch(setHideSpinner());
                                          console.log(res);
                                          setListState(INIT);
                                          message.success(
                                            "Xóa người dùng thành công"
                                          );
                                        })
                                        .catch((err) => {
                                          dispatch(setHideSpinner());
                                          console.log(err);
                                          message.error(
                                            err.response.data.content
                                          );
                                        });
                                    }}
                                    className="bg-red-500 hover:bg-red-800  text-white rounded-md  hover:text-white"
                                  >
                                    <DeleteOutlined className=" p-2" />
                                  </button>
                                </td>
                              </tr>
                            );
                          })}
                        </tbody>
                      </table>
                    );
                  }}
                >
                  <Avatar key={index} src={member.avatar} />
                </Popover>
              );
            })}
            {record.members?.length > 2 ? (
              <Popover
                placement="top"
                title="All members"
                content={() => {
                  return record.members?.map((item, index) => {
                    return <Avatar key={index} src={item.avatar} />;
                  });
                }}
              >
                <Avatar style={{ background: "#DDDDDD", color: "black" }}>
                  ...
                </Avatar>
              </Popover>
            ) : (
              ""
            )}
            <Popover
              placement="topLeft"
              title={"Add user"}
              content={() => {
                return (
                  <AutoComplete
                    autoFocus
                    value={value}
                    style={{ width: "100%" }}
                    options={userOptions.map((x) => ({
                      label: x.name,
                      value: x.userId.toString(),
                    }))}
                    onSearch={searchDelayAction}
                    onSelect={(_, option) => {
                      setValue(option.label);

                      const projectId = record.id;
                      const userId = option.value;

                      userService
                        .assignUserProject({
                          projectId,
                          userId,
                        })
                        .then((res) => {
                          console.log("res", res);

                          setListState(INIT);
                          message.success("Thêm người dùng thành công");
                        })
                        .catch((err) => {
                          console.log(err);

                          const errors = {
                            "User is unthorization!":
                              "Không thể thêm người dùng vào dự án của người khác",
                            "User already exists in the project!":
                              "Đã tồn tại người dùng, vui lòng chọn người dùng khác",
                          };

                          const content = errors[err.response.data.content];

                          message.error(content || "Thêm không thành công!");
                        })
                        .finally(() => {
                          setValue("");
                          setUserOptions([]);
                        });
                    }}
                    onChange={(text) => {
                      setValue(text);
                    }}
                    placeholder="add user..."
                  />
                );
              }}
            >
              <Button className="px-3 text-blue-500 rounded-full">+</Button>
            </Popover>
          </div>
        );
      },
    },
    {
      title: "Action",
      key: "action",
      render: (_, record) => (
        <Space size="middle">
          <button
            onClick={() => {
              // dispatch lên reducer nội dụng drawer
              dispatch(
                openFormEdit(
                  <FormEditProject
                    onSuccess={() => {
                      setListState(INIT);
                    }}
                  />,
                  "Edit Project"
                )
              );
              // dispatch dữ liệu dòng hiện tại lên reducer
              dispatch(editProject(record));
            }}
            className="bg-teal-600 hover:bg-teal-800  text-white rounded-md  hover:text-white"
            style={{ fontSize: 14 }}
          >
            <FormOutlined className="p-2" />
          </button>
          <DeleteProject
            projectId={record.id}
            onSuccess={() => {
              setListState(INIT);
            }}
          />
        </Space>
      ),
    },
  ];
  return (
    <div className="w-2/3 mt-10 ml-16">
      <Space
        style={{
          marginBottom: 16,
        }}
      >
        <Button onClick={setAgeSort}>Sort age</Button>
        <Button onClick={clearFilters}>Clear filters</Button>
        <Button onClick={clearAll}>Clear filters and sorters</Button>
      </Space>
      <Table
        columns={columns}
        rowKey={"id"}
        dataSource={dataRaw}
        onChange={handleChange}
      />
    </div>
  );
};

export default ProjectManagement;
