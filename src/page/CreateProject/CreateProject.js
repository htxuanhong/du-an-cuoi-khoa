import { message } from "antd";
import { useFormik } from "formik";
import React, { useEffect, useState } from "react";
import { useDispatch } from "react-redux";
import { setHideSpinner, setShowSpinner } from "../../redux/action/action";
import { projectService } from "../../service/projectService";
export default function CreateProject() {
  const [state, setstate] = useState([]);
  const dispatch = useDispatch();
  useEffect(() => {
    projectService
      .getProjectCategory()
      .then((res) => {
        console.log(res);
        setstate(res.data.content);
      })
      .catch((err) => {
        console.log(err);
      });
  }, []);

  let handleSlection = () => {
    return state.map((item, index) => {
      return (
        <option value={item.id} id={item.id} key={index}>
          {item.projectCategoryName}
        </option>
      );
    });
  };

  // formik lấy thông tin dữ liệu từ người dùng nhập vào
  const formik = useFormik({
    initialValues: {
      projectName: "",
      description: "",
      categoryId: "",
    },
    onSubmit: (values) => {
      dispatch(setShowSpinner());
      projectService
        .postCreateProjectAuthorization(values)
        .then((res) => {
          dispatch(setHideSpinner());
          console.log("gui len thanh cong", res);
          message.success("Thêm thành công");
        })
        .catch((err) => {
          dispatch(setHideSpinner());
          console.log(err);
          message.error(err.response.data.content);
        });
      console.log(values);
    },
  });
  const handleEditorChange = (content, editor) => {
    console.log("content", content); //
    // setFieldValue(" description", content);
  };
  return (
    <div className="container">
      <div className="bg-slate-100 w-2/3 h-max ml-48  ">
        <h1 className="text-lg text-left font-medium text-gray-800">
          CreateProject
        </h1>
        <form onSubmit={formik.handleSubmit}>
          <div className="form-group">
            <label htmlFor="projectName">Name</label>
            <input
              id="projectName"
              name="projectName"
              type="text"
              className="form-control"
              placeholder="Project name"
              onChange={formik.handleChange}
              value={formik.values.projectName}
            />
          </div>
          <div className="form-group">
            <label htmlFor="exampleFormControlTextarea1">Description</label>
            {/* 
            <Editor
              id="description"
              type="text"
              value={formik.values.description}

              onChange={formik.handleChange} // onChange của thư viện formik 
             onChange={handleEditorChange}   // onChange của thư viện tinymce-react


              name="description"
              initialValue="<p>This is the initial content of the editor</p>"
              init={{
                plugins: "link image code",
                toolbar:
                  "undo redo | bold italic | alignleft aligncenter alignright | code",
              }}
              
            /> */}

            <textarea
              id="description"
              name="description"
              type="text"
              className="form-control"
              placeholder="description"
              onChange={formik.handleChange}
              value={formik.values.description}
              rows="3"
            ></textarea>
          </div>

          <div className="form-group">
            <label htmlFor="projectName">Project Category</label>
            <select
              className="form-control"
              name="categoryId"
              id="categoryId"
              onChange={formik.handleChange}
              value={formik.values.categoryId}
            >
              {" "}
              {handleSlection()}
            </select>
          </div>

          <button
            type="submit"
            class="inline-block px-6 py-2.5 bg-blue-400 text-white font-medium text-xs leading-tight uppercase rounded shadow-md hover:bg-blue-500 hover:shadow-lg focus:bg-blue-500 focus:shadow-lg focus:outline-none focus:ring-0 active:bg-blue-600 active:shadow-lg transition duration-150 ease-in-out"
          >
            Create Project
          </button>
        </form>
      </div>
    </div>
  );
}
