import { BarsOutlined, PlusOutlined, SearchOutlined } from "@ant-design/icons";
import { Layout, Menu } from "antd";
import React, { useState } from "react";
import { useDispatch } from "react-redux";
import { openFormCreateTask } from "../redux/action/action";
import FormCreateTask from "./FormCreateTask/FormCreateTask";
const { Sider } = Layout;
export default function Sildebar() {
  const dispatch = useDispatch();
  const [state, setState] = useState({
    collapsed: false,
  });
  const toggle = () => {
    setState({
      collapsed: !state.collapsed,
    });
  };
  return (
    <div>
      <Sider
        trigger={null}
        collapsible
        collapsed={state.collapsed}
        style={{ height: "100%" }}
      >
        <div className="text-right pr-2" onClick={toggle}>
          <BarsOutlined className="cursor-pointer text-white w-8 h-8 text-2xl py-4" />
        </div>
        <Menu theme="dark" mode="inline" defaultSelectedKeys={["1"]}>
          <Menu.Item
            key="1"
            onClick={() => {
              dispatch(openFormCreateTask(<FormCreateTask />, "Create task"));
            }}
            icon={<PlusOutlined style={{ fontSize: 20 }} />}
          >
            <span className="mb-2">Create task</span>
          </Menu.Item>
          <Menu.Item key="2" icon={<SearchOutlined style={{ fontSize: 20 }} />}>
            <span className="mb-2">Search</span>
          </Menu.Item>
        </Menu>
      </Sider>
    </div>
  );
}

// {/* <div className="sideBar">
/* <div className="sideBar-top">
  <div className="sideBar-icon">
    <i className="fab fa-jira" />
  </div>

  <div className="sideBar-icon">
    <i className="fa fa-plus" />
    <span className="title">Create task</span>
  </div>
  <div
    className="sideBar-icon"
    data-toggle="modal"
    data-target="#searchModal"
    style={{ cursor: "pointer" }}
  >
    <SearchOutlined />
    <span className="title">Search</span>
  </div>
</div>
<div className="sideBar-bottom">
  <div className="sideBar-icon">
    <i className="fa fa-question-circle" />
    <span className="title">ABOUT</span>
  </div>
</div>
</div> */
