import { Editor } from "@tinymce/tinymce-react";
import { message } from "antd";
import { withFormik } from "formik";
import React, { useEffect, useState } from "react";
import { connect, useDispatch } from "react-redux";
import * as Yup from "yup";
import {
  closeDrawer,
  setHideSpinner,
  setShowSpinner,
  setSubmitEditProject,
} from "../../redux/action/action";
import { projectService } from "../../service/projectService";

function FormEditProject(props) {
  const [state, setState] = useState([]);
  const { values, handleChange, setFieldValue, handleSubmit } = props;

  const dispatch = useDispatch();

  useEffect(() => {
    // gọi API load projectCategory
    dispatch(setShowSpinner());
    projectService
      .getProjectCategory()
      .then((res) => {
        dispatch(setHideSpinner());
        console.log(res);
        setState(res.data.content);
      })
      .catch((err) => {
        dispatch(setHideSpinner());

        console.log(err);
      });
    //   load sự kiện submit lên drawer nut submit
    dispatch(setSubmitEditProject(handleSubmit));
  }, []);

  const handleEditorChange = (content) => {
    setFieldValue("description", content);
  };
  let handleSlection = () => {
    return state.map((item, index) => {
      return (
        <option value={item.id} id={item.id} key={index}>
          {item.projectCategoryName}
        </option>
      );
    });
  };

  return (
    <form className="container" onSubmit={handleSubmit}>
      <div className="row">
        <div className="col-4">
          <div class="form-group">
            <label className="font-bold">Project id</label>
            <input
              disabled
              value={values.id}
              className="form-control"
              name="id"
            />
          </div>
        </div>
        <div className="col-4">
          <div class="form-group">
            <label className="font-bold">Project name</label>
            <input
              value={values.projectName}
              className="form-control"
              name="projectName"
              onChange={handleChange}
            />
          </div>
        </div>
        <div className="col-4">
          <div class="form-group">
            <label className="font-bold">Project Category</label>
            <select
              className="form-control"
              name="categoryId"
              value={values.categoryId}
            >
              {handleSlection()}
            </select>
          </div>
        </div>
        <div className="col-12">
          <div class="form-group">
            <label className="font-bold">Description</label>
            <Editor
              name="description123"
              value={values.description}
              init={{
                selector: "textarea#myTextArea",
                height: 450,
                menubar: false,
                plugins: [
                  "advlist autolink lists link image charmap print preview anchor",
                  "searchreplace visualblocks code fullscreen",
                  "insertdatetime media table paste code help wordcount",
                ],
                toolbar:
                  "undo redo | formatselect | " +
                  "bold italic backcolor | alignleft aligncenter " +
                  "alignright alignjustify | bullist numlist outdent indent | " +
                  "removeformat | help",
              }}
              onEditorChange={handleEditorChange}
            />
          </div>
        </div>
      </div>
    </form>
  );
}

const EditProjectForm = withFormik({
  enableReinitialize: true,
  mapPropsToValues: (props) => {
    const { projectEdit } = props;

    return {
      id: projectEdit?.id,
      projectName: projectEdit.projectName,
      description: projectEdit.description,
      categoryId: projectEdit.categoryId,
    };
  },
  validationSchema: Yup.object().shape({}),
  handleSubmit: (values, { props, setSubmitting }) => {
    let newValues = { ...values };
    projectService
      .updateProject(newValues)
      .then((res) => {
        console.log(res);
        message.success("Xử lý thành công");

        props.onSuccess?.();
      })
      .catch((err) => {
        console.log(err);
      });
    console.log("handleSubmit props", props);
    props.closeDrawerAction();
  },
  displayName: "EditProjectForm",
})(FormEditProject);

const mapStateToProps = (state) => ({
  projectEdit: state.ProjectReducer.projectEdit,
});

const mapDispatchToProps = () => (dispatch) => {
  return {
    closeDrawerAction: () => dispatch(closeDrawer()),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(EditProjectForm);
