import React from "react";

export default function Menu() {
  return (
    <div className="menu">
      <div className="account">
        <div className="avatar">
          <img src={require("../assets/img/download.jfif")} alt />
        </div>
        <div className="account-info">
          <p>BC03-JIRA.VN</p>
          <p>Report bugs</p>
        </div>
      </div>
      <div className="control">
        <div className="active">
          <i className="fa fa-credit-card" />
          <a href="/CyberBoard">
            <span className="text-base font-semibold text-slate-800 hover:text-white">
              Cyber Board
            </span>
          </a>
        </div>
        <div className="active">
          <i className="fa fa-cog" />
          <a href="/CreateProject">
            <span className="text-base font-semibold text-slate-800 hover:text-white">
              Create Project
            </span>
          </a>
        </div>
        <div className="active">
          <i className="fa fa-cog" />
          <a href="/ProjectManagement">
            <span className="text-base font-semibold text-slate-800 hover:text-white">
              Project Manament
            </span>
          </a>
        </div>
      </div>
      <div className="feature">
        <div>
          <i className="fa fa-truck" />
          <span>Releases</span>
        </div>
        <div>
          <i className="fa fa-equals" />
          <span>Issues and filters</span>
        </div>
        <div>
          <i className="fa fa-paste" />
          <span>Pages</span>
        </div>
        <div>
          <i className="fa fa-location-arrow" />
          <span>Reports</span>
        </div>
        <div>
          <i className="fa fa-box" />
          <span>Components</span>
        </div>
      </div>
    </div>
  );
}
