import { Editor } from "@tinymce/tinymce-react";
import { Select, Slider } from "antd";
import React, { useState } from "react";

const { Option } = Select;
const children = [];
for (let i = 10; i < 36; i++) {
  children.push(<Option key={i.toString(36) + i}>{i.toString(36) + i}</Option>);
}

const handleChange = (value) => {
  console.log(`Selected: ${value}`);
};
export default function FormCreateTask() {
  const [size, setSize] = useState("middle");

  const [timeTracking, setTimeTracking] = useState({
    timeTrackingSpent: 0,
    timeTrackingRemaining: 0,
  });
  const hanleEditorChange = () => {};
  return (
    <div className="container">
      <div className="form-group">
        <label className="font-bold">Project</label>
        <select name="projectId" className="form-control">
          <option value="54">Project A</option>
          <option value="55">Project B</option>
        </select>
      </div>
      <div className="form-group">
        <div className="row">
          <div className="col-6">
            <label className="font-bold">Priority</label>
            <select name="priorityId" className="form-control">
              <option value="">High</option>
              <option value="">Low</option>
            </select>
          </div>
          <div className="col-6">
            <label className="font-bold">Task type</label>
            <select name="typeId" className="form-control">
              <option value="">New Task</option>
              <option value="">Bugs</option>
            </select>
          </div>
        </div>
      </div>
      <div className="form-group">
        <div className="row">
          <div className="col-6">
            <label className="font-bold">Assignees</label>
            <Select
              mode="multiple"
              size={size}
              placeholder="Please select"
              defaultValue={["a10", "c12"]}
              onChange={handleChange}
              style={{ width: "100%" }}
            >
              {children}
            </Select>
            <div className="row" style={{ marginTop: "10px" }}>
              <div className="col-12">
                <label className="font-bold mt-4">Original estimate</label>
                <input
                  type="number"
                  defaultValue="0"
                  min="0"
                  className="form-control "
                  name="originalEstimate"
                />
              </div>
            </div>
          </div>
          <div className="col-6">
            <label className="font-bold">Time tracking</label>
            <Slider
              defaultValue={30}
              value={timeTracking.timeTrackingSpent}
              max={
                Number(timeTracking.timeTrackingSpent) +
                Number(timeTracking.timeTrackingRemaining)
              }
            />
            <div className="row">
              <div
                className="col-6 text-left font-bold"
                style={{ color: "#2c7da0" }}
              >
                {timeTracking.timeTrackingSpent}h logged
              </div>
              <div
                className="col-6 text-right font-bold"
                style={{ color: "#2c7da0" }}
              >
                {timeTracking.timeTrackingRemaining}h remainning
              </div>
            </div>
            <div className="row " style={{ marginTop: "5px" }}>
              <div className="col-6">
                <label className="font-bold">Time spent</label>
                <input
                  type="number"
                  defaultValue="0"
                  min="0"
                  className="form-control "
                  name="timeTrackingSpent"
                  onChange={(e) => {
                    setTimeTracking({
                      ...timeTracking,
                      timeTrackingSpent: e.target.value,
                    });
                  }}
                />
              </div>
              <div className="col-6">
                <label className="font-bold">Time remaining</label>
                <input
                  defaultValue="0"
                  min="0"
                  type="number"
                  className="form-control "
                  name="timeTrackingRemaining"
                  onChange={(e) => {
                    setTimeTracking({
                      ...timeTracking,
                      timeTrackingRemaining: e.target.value,
                    });
                  }}
                />
              </div>
            </div>
          </div>
        </div>
      </div>
      <div className="form-group">
        <label className="font-bold">Description</label>
        <Editor
          name="description"
          init={{
            selector: "textarea#myTextArea",
            height: 400,
            menubar: false,
            plugins: [
              "advlist autolink lists link image charmap print preview anchor",
              "searchreplace visualblocks code fullscreen",
              "insertdatetime media table paste code help wordcount",
            ],
            toolbar:
              "undo redo | formatselect | " +
              "bold italic backcolor | alignleft aligncenter " +
              "alignright alignjustify | bullist numlist outdent indent | " +
              "removeformat | help",
          }}
          onEditorChange={hanleEditorChange}
        />
      </div>
    </div>
  );
}
