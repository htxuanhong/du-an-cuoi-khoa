import { Button, Drawer } from "antd";
import React from "react";
import { useDispatch, useSelector } from "react-redux";
import { closeDrawer } from "../../redux/action/action";

const DrawerBugs = (props) => {
  const { visiable, componentContentDrawer, callBackSubmit, title } =
    useSelector((state) => state.drawerReducer);

  console.log("visiable", visiable);

  const dispatch = useDispatch();

  const onClose = () => {
    dispatch(closeDrawer());
  };

  return (
    <>
      <Drawer
        title={title}
        onClose={onClose}
        width={720}
        visible={visiable}
        bodyStyle={{
          paddingBottom: 80,
        }}
        footer={
          <div style={{ textAlign: "right" }}>
            <Button onClick={onClose} style={{ marginRight: 8 }}>
              Cancel
            </Button>
            <Button
              onClick={callBackSubmit}
              className="bg-teal-600 hover:bg-teal-800 border border-teal-800 text-white hover:text-white "
            >
              Submit
            </Button>
          </div>
        }
      >
        {/* Nội dung thay đổi của drawer */}
        {componentContentDrawer}
      </Drawer>
    </>
  );
};

export default DrawerBugs;
