import React from "react";
import Main from "../component/Main";
import Content from "../component/MainComponent/Content";
import Header from "../component/MainComponent/Header";
import Info from "../component/MainComponent/Info";
import Menu from "../component/Menu";
import Modal from "../component/Modal/Modal";
import Sildebar from "../component/Sildebar";

export default function LayoutThem({ Component }) {
  return (
    <div className="jira">
      <Sildebar />
      <Menu />

      <Component />
    </div>
  );
}
