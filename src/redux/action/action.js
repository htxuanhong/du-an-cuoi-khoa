import {
  CLOSE_DRAWER,
  EDIT_PROJECT,
  OPEN_DRAWER,
  OPEN_FORM_CREATE_TASK,
  OPEN_FORM_EDIT_PROJECT,
  SET_HIDE_SPINNER,
  SET_SHOW_SPINNER,
  SET_SUBMIT_EDIT_PROJECT,
} from "../contants/contants";

export const openDrawer = () => {
  return { type: OPEN_DRAWER };
};

export const closeDrawer = () => {
  return { type: CLOSE_DRAWER };
};

export const openFormEdit = (component, title) => {
  return {
    type: OPEN_FORM_EDIT_PROJECT,
    payload: component,
    title: title,
  };
};
export const setSubmitEditProject = (submitFuntion) => {
  return { type: SET_SUBMIT_EDIT_PROJECT, payload: submitFuntion };
};

export const editProject = (projectEditModal) => {
  return { type: EDIT_PROJECT, payload: projectEditModal };
};

export const setShowSpinner = () => {
  return { type: SET_SHOW_SPINNER };
};

export const setHideSpinner = () => {
  return { type: SET_HIDE_SPINNER };
};

export const openFormCreateTask = (component, title) => {
  return { type: OPEN_FORM_CREATE_TASK, payload: component, title: title };
};
