import React from "react";
import {
  CLOSE_DRAWER,
  OPEN_DRAWER,
  OPEN_FORM_CREATE_TASK,
  OPEN_FORM_EDIT_PROJECT,
  SET_SUBMIT_EDIT_PROJECT,
} from "../contants/contants";
const initialState = {
  visiable: false,
  title: "",
  componentContentDrawer: <p>default drawer content</p>,
  callBackSubmit: (propsValue) => {
    alert("click demo!");
  },
};

export const drawerReducer = (state = initialState, action) => {
  switch (action.type) {
    case OPEN_DRAWER:
      return { ...state, visiable: true };
    case CLOSE_DRAWER:
      return { ...state, visiable: false };

    case OPEN_FORM_EDIT_PROJECT:
      return {
        ...state,
        visiable: true,
        title: action.title,
        componentContentDrawer: action.payload,
      };
    case SET_SUBMIT_EDIT_PROJECT: {
      state.callBackSubmit = action.payload;
      return { ...state };
    }
    case OPEN_FORM_CREATE_TASK: {
      return {
        ...state,
        visiable: true,
        componentContentDrawer: action.payload,
        title: action.title,
      };
    }
    default:
      return state;
  }
};
