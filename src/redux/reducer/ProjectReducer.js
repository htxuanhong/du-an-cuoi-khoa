import { DELETE_PROJECT, EDIT_PROJECT } from "../contants/contants";

const initialState = {
  projectEdit: {
    id: 0,
    projectName: "string",
    creator: 0,
    description: "string",
    categoryId: "1",
  },
};

export const ProjectReducer = (state = initialState, action) => {
  switch (action.type) {
    case EDIT_PROJECT: {
      state.projectEdit = action.payload;
      return { ...state };
    }
    case DELETE_PROJECT: {
    }
    default:
      return state;
  }
};
