import { combineReducers } from "redux";
import { drawerReducer } from "./DrawerBugReducer";
import { ProjectReducer } from "./ProjectReducer";
import { spinnerReducer } from "./spinnerReducer";

export const rootReducer = combineReducers({
  drawerReducer,
  ProjectReducer,
  spinnerReducer,
});
