import LayoutThem from "../HOC/LayoutThem";
import CreateProject from "../page/CreateProject/CreateProject";
import CyberBoard from "../page/CyberBoard/CyberBoard";
import LoginPage from "../page/LoginPage/LoginPage";
import NotFound from "../page/NotFound/NotFound";
import ProjectDetail from "../page/ProjectDetailCyber/ProjectDetailCyber";
import ProjectManagement from "../page/ProjectManagement/ProjectManagement";
import ProjectSetting from "../page/ProjectSetting/ProjectSetting";

// mảng chứa các object qua app.js map ra
export const userRoutes = [
  {
    path: "/",
    component: <LayoutThem Component={ProjectManagement} />,
    exact: true, // không hiểu
    isUseLayout: true,
  },

  {
    path: "/CyberBoard",
    component: <LayoutThem Component={CyberBoard} />,
    isUseLayout: true,
  },
  {
    path: "/ProjectSetting",
    component: <LayoutThem Component={ProjectSetting} />,
    isUseLayout: true,
  },
  {
    path: "/CreateProject",
    component: <LayoutThem Component={CreateProject} />,
    isUseLayout: true,
  },
  {
    path: "/ProjectManagement",
    component: <LayoutThem Component={ProjectManagement} />,
    isUseLayout: true,
  },
  {
    path: "/projectdetail/:projectId",
    component: <LayoutThem Component={ProjectDetail} />,
    isUseLayout: true,
  },
  {
    path: "/login",
    component: LoginPage,
  },
  {
    path: "*",
    component: NotFound,
  },
];
