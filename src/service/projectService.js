import axios from "axios";
import { ACCESSTOKEN, localStorageService } from "./localStorageService";

export const projectService = {
  getProjectCategory: () => {
    return axios({
      method: "GET",
      url: `https://casestudy.cyberlearn.vn/api/ProjectCategory`,
    });
  },
  postCreateProject: (values) => {
    return axios({
      method: "POST",
      url: `https://casestudy.cyberlearn.vn/api/Project/createProject`,
      data: values,
    });
  },
  postCreateProjectAuthorization: (values) => {
    return axios({
      url: `https://casestudy.cyberlearn.vn/api/Project/createProjectAuthorize`,
      method: "POST",
      data: values,
      headers: {
        Authorization:
          "Bearer " + localStorageService.getUserInfor(ACCESSTOKEN),
        // "eyJhbGciOiJodHRwOi8vd3d3LnczLm9yZy8yMDAxLzA0L3htbGRzaWctbW9yZSNobWFjLXNoYTI1NiIsInR5cCI6IkpXVCJ9.eyJodHRwOi8vc2NoZW1hcy54bWxzb2FwLm9yZy93cy8yMDA1LzA1L2lkZW50aXR5L2NsYWltcy9lbWFpbGFkZHJlc3MiOiJuZ3V5ZW52YW5uaGF0MTQxOTlAZ21haWwuY29tIiwibmJmIjoxNjYwNTA0MjUwLCJleHAiOjE2NjA1MDc4NTB9.9YyLC2NtwOir_tAIhtRPE7jBpE0Pa6PA7ln23If2KAc",
      },
    });
  },
  getListProject: () => {
    return axios({
      method: "GET",
      url: `https://casestudy.cyberlearn.vn/api/Project/getAllProject`,
      headers: {
        Authorization:
          "Bearer " + localStorageService.getUserInfor(ACCESSTOKEN),
      },
    });
  },
  updateProject: (projectUpdate) => {
    return axios({
      method: "PUT",
      url: `https://casestudy.cyberlearn.vn/api/Project/updateProject?projectId=${projectUpdate.id}`,
      data: projectUpdate,
      headers: {
        Authorization:
          "Bearer " + localStorageService.getUserInfor(ACCESSTOKEN),
      },
    });
  },
  deleteProject: (id) => {
    return axios({
      method: "DELETE",
      url: `https://casestudy.cyberlearn.vn/api/Project/deleteProject?projectId=${id}`,
      headers: {
        Authorization:
          "Bearer " + localStorageService.getUserInfor(ACCESSTOKEN),
      },
    });
  },
  getProjectDetail: (projectId) => {
    return axios({
      method: "GET",
      url: `https://casestudy.cyberlearn.vn/api/Project/getProjectDetail?id=${projectId}`,
      headers: {
        Authorization:
          "Bearer " + localStorageService.getUserInfor(ACCESSTOKEN),
      },
    });
  },
};
